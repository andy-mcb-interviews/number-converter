package mcbrearty.andrew.converter;

/**
 * Convertible Number representation for Integers.
 * 
 * @author amcbrearty
 */
public enum ConvertibleInteger implements ConvertibleNumber<Integer>{
	
	zero(0),
	one(1),
	two(2),
	three(3),
	four(4),
	five(5),
	six(6),
	seven(7),
	eight(8),
	nine(9),
	ten(10),
	eleven(11),
	twelve(12),
	thirteen(13),
	fourteen(14),
	fifteen(15),
	sixteen(16),
	seventeen(17),
	eighteen(18),
	nineteen(19),
	twenty(20),
	thirty(30),
	forty(40),
	fifty(50),
	sixty(60),
	seventy(70),
	eighty(80),
	ninety(90);
	
	private Integer number;
	private String text;
	
	ConvertibleInteger(Integer number) {
		this.number = number;
		this.text = this.name();
	}

	public Integer asNumber() {
		return number;
	}
	
	public String asText() {
		return text;
	}
}
