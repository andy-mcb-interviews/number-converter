package mcbrearty.andrew.converter;

/**
 * Defines the operations that a convertible number exposes to enable the 
 * retrieval of either the number value or the text value representation. 
 *
 * @param <T> the Number type the implementor supports
 * 
 * @author amcbrearty
 */
public interface ConvertibleNumber<T extends Number> {
	
	/**
	 * The number value for this convertible number.
	 * @return the number
	 */
	T asNumber();
	
	/**
	 * The text value for this convertible number.
	 * @return the text
	 */
	String asText();
	

}
