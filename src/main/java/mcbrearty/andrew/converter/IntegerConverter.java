package mcbrearty.andrew.converter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * Take a number and give the equivalent number in british english words.
 * 
 * <code>
 * 	1 = one
 *  21 = twenty one
 *  105 = one hundred and five
 *  56945781 = fifty six million nine hundred and forty five thousand seven hundred and eighty one
 * </code>
 * 
 * This implementation limits conversions to the range -999,999,999 to 999,999,999
 * 
 * Assumptions from requirements: Bottom limit is top limit multiplied by
 * minus one. Words are lower case seperated by single space.
 * 
 * @author amcbrearty
 */
public class IntegerConverter implements NumberConverter<Integer> {

	/** max value provided in requirement */
	public static final int MAX_CONVERTIBLE_VALUE = 999999999;

	/** assumed minimum value */
	public static final int MIN_CONVERTIBLE_VALUE = -999999999;

	/** map of numbers supported by this converter */
	private static final Map<Integer, ConvertibleInteger> numbers;

	/** static initialiser for map of supported integers */
	static {
		numbers = new HashMap<Integer, ConvertibleInteger>();

		for (ConvertibleInteger number : ConvertibleInteger.values()) {
			numbers.put(number.asNumber(), number);
		}
	}

	/**
	 * Converts integer objects into their corresponding text representation.
	 * 
	 * @inheritDoc
	 */
	public String convert(Integer numberToConvert) {
		validate(numberToConvert);
	
		StringBuffer conversion = new StringBuffer();
		
		if (numberToConvert < 0) {
			numberToConvert = numberToConvert * -1;
			conversion.append("minus ");
		}
		
		String result = reduce(numberToConvert, conversion);
		
		return result.trim();
	}


	/**
	 * Seed the reduce method with the number to be converted and the initial string buffer 
	 * to store the text conversion.
	 *  
	 * @param numberAsInt the number to be converted to text
	 * @param conversion the string buffer containing the text conversion
	 * 
	 * @return the text representation of the provided number
	 */
	protected String reduce(int numberAsInt, StringBuffer conversion) {
		List<IntegerFactor> factorList = Arrays.asList(IntegerFactor.values());
		
		return reduce(numberAsInt, conversion, factorList.listIterator());
	}

	/**
	 * Recursively called method to reduce the provided number into recognisable
	 * integer values that can be found in the integer list and therefore converted 
	 * to their text representation.
	 * 
	 * Uses the list of convertible factors to incrementally reduce the factor 
	 * by which the provided number is divided, enabling the correct factor text value 
	 * to be used e.g. factor of 600 = hundred.
	 * 
	 * @param numberAsInt the number to be converted to text
	 * @param conversion string buffer containing the text representation
	 * @param factorList list of factors supported by this converter
	 * @return the text representation of the provided number
	 * 
	 * @see IntegerFactor
	 */
	protected String reduce(int numberAsInt, StringBuffer conversion, ListIterator<IntegerFactor> factorList) {
		// find the provided number in the list of supported integers 
		ConvertibleInteger convertibleNumber = numbers.get(numberAsInt);
		
		if (convertibleNumber != null) {
			conversion.append(convertibleNumber.name());
			
		} else {
			// when the provided number is not found reduce into 
			// constituent parts to enable further recursive calls
			
			IntegerFactor factor = factorList.next();
			
			int majorBit = Math.abs(numberAsInt / factor.factor());
			int minorBit = numberAsInt % factor.factor();

			// special case for factor ten where upscale is required
			// to retrieve the correct text value
			if (IntegerFactor.ten.equals(factor)) {
				majorBit = majorBit * factor.factor();
				conversion.append(numbers.get(majorBit));
				
			} else if (majorBit != 0) {
				reduce(majorBit, conversion);
				conversion.append(factor.text());
			}
			
			if (minorBit != 0) {
				if (majorBit != 0 && minorBit < 100) {
					conversion.append(factor.join());
				}
				reduce(minorBit, conversion, factorList);
			}
		}

		return trimSpaces(conversion.toString());
	}
	
	/**
	 * Helper that replaces all spaces between words with a single space in the provided text.
	 * 
	 * Also removes leading and trailing spaces.
	 * 
	 * @param toTrim the text to replace spaces in
	 * @return the text with spaces replaced
	 */
	protected String trimSpaces(String toTrim) {
		String trimmed = toTrim.trim();
		String result = trimmed.replaceAll("  ", " ");
		
		return result;
	}
	
	/**
	 * Confirms the provided number is within the acceptable limits for this converter.
	 * 
	 * @param numberToValidate the number to be validated
	 * @throws IllegalArgumentException when the number fails the validation
	 */
	protected void validate(int numberToValidate) throws IllegalArgumentException {
		if (numberToValidate > MAX_CONVERTIBLE_VALUE || numberToValidate < MIN_CONVERTIBLE_VALUE)
			throw new IllegalArgumentException(
					"Value to convert must be within the range "
							+ MIN_CONVERTIBLE_VALUE + " to "
							+ MAX_CONVERTIBLE_VALUE);
	}
}
