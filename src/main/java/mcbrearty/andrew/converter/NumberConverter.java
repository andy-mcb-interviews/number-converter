package mcbrearty.andrew.converter;

/**
 * Defines the operations that number converter implementations should expose.
 * 
 * Implementations should throw IllegalArgumentException when a call is made to convert for
 * a value that is not supported by the converter.
 * 
 * @author amcbrearty
 *
 * @param <T> the Number type the implementor supports
 */
public interface NumberConverter<T extends Number> {

	/**
	 * Converts the provided number of type T into its text representation.
	 * 
	 * @param numberToConvert the number to be converted to text
	 * @return the text representation of the provided number
	 * @throws IllegalArgumentException when called with a value not accepted by the converter
	 */
	String convert(T numberToConvert) throws IllegalArgumentException;

}