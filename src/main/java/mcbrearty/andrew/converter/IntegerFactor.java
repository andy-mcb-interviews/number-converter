package mcbrearty.andrew.converter;

/**
 * Represents an Integer convertible factor.
 * 
 * @inheritDoc
 * @author amcbrearty
 */
public enum IntegerFactor implements ConvertibleFactor<Integer> {
	
	million(1000000, " and ", " million "),
	thousand(1000, " and ", " thousand "),
	hundred(100, " and ", " hundred "),
	ten(10, " ", "");
	
	private Integer factor;
	private String joinText;
	private String text;
	
	IntegerFactor(Integer factor, String joinText, String text) {
		this.factor = factor;
		this.joinText = joinText;
		this.text = text;
	}

	public Integer factor() {
		return factor;
	}

	public String join() {
		return joinText;
	}

	public String text() {
		return text;
	}

}
