package mcbrearty.andrew.converter;

/**
 * Provides the values used when determining the factor for a particular number e.g.
 * factor 100 = hundred.
 * 
 * The number 100 can be thought of as 2 parts; the integer one; and the factor hundred.
 * 
 * Additionally provides methods to retrieve the join text that should be used e.g.
 * 101 = integer one; factor hundred; join and; integer one = one hundred and one
 *  
 * @author amcbrearty
 *
 * @param <T> the number type supported by the implementor
 */
public interface ConvertibleFactor<T extends Number> {

	/**
	 * The factor value of type T.
	 * @return the factor value
	 */
	T factor();

	/**
	 * The join text for this factor.
	 * @return the join text e.g. and
	 */
	String join();

	/**
	 * The text representation for this factor e.g. hundred
	 * @return the text representation for this factor
	 */
	String text();

}
