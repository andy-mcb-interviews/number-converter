package mcbrearty.andrew.converter;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Test conversion of digits 1..9 only.
 * 
 * @author amcbrearty
 */
public class DigitTest {
	
	private NumberConverter<Integer> integerConverter = new IntegerConverter();

	@Test
	public void convertOne() {
		int numberAsInt = 1;
		String numberAsWord = "one";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertTwo() {
		int numberAsInt = 2;
		String numberAsWord = "two";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertThree() {
		int numberAsInt = 3;
		String numberAsWord = "three";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertFour() {
		int numberAsInt = 4;
		String numberAsWord = "four";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertFive() {
		int numberAsInt = 5;
		String numberAsWord = "five";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertSix() {
		int numberAsInt = 6;
		String numberAsWord = "six";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertSeven() {
		int numberAsInt = 7;
		String numberAsWord = "seven";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertEight() {
		int numberAsInt = 8;
		String numberAsWord = "eight";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertNine() {
		int numberAsInt = 9;
		String numberAsWord = "nine";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
}
