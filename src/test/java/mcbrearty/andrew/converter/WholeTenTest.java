package mcbrearty.andrew.converter;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Test conversion of whole tens only e.g. 20,30..70,80 etc.
 * 
 * @author amcbrearty
 */
public class WholeTenTest {

	private NumberConverter<Integer> integerConverter = new IntegerConverter();

	@Test
	public void convertTwenty() {
		int numberAsInt = 20;
		String numberAsWord = "twenty";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertThirty() {
		int numberAsInt = 30;
		String numberAsWord = "thirty";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertForty() {
		int numberAsInt = 40;
		String numberAsWord = "forty";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertfifty() {
		int numberAsInt = 50;
		String numberAsWord = "fifty";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertSixty() {
		int numberAsInt = 60;
		String numberAsWord = "sixty";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertSeventy() {
		int numberAsInt = 70;
		String numberAsWord = "seventy";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertEighty() {
		int numberAsInt = 80;
		String numberAsWord = "eighty";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertNinety() {
		int numberAsInt = 90;
		String numberAsWord = "ninety";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
}
