package mcbrearty.andrew.converter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.Assert.assertEquals;

/**
 * Test conversion of numbers as provided by the examples in the requirements.
 * 
 * @author amcbrearty
 */
@RunWith(Parameterized.class)
public class IntegerConverterTest {

    @Parameterized.Parameters(name = "{0}: {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { 0, "zero" },
                { 1, "one" },
                {21, "twenty one"},
                {105, "one hundred and five"},
                {123, "one hundred and twenty three"},
                {1005, "one thousand and five"},
                {1042, "one thousand and forty two"},
                {1105, "one thousand one hundred and five"},
                {56945781, "fifty six million nine hundred and forty five thousand seven hundred and eighty one"},
                {999999999, "nine hundred and ninety nine million nine hundred and ninety nine thousand nine hundred and ninety nine"}
        });
    }

    private NumberConverter<Integer> integerConverter = new IntegerConverter();

    @Parameterized.Parameter(value = 0)
    public int numberAsInt;

    @Parameterized.Parameter(value = 1)
    public String numberAsWord;

    @Test
	public void shouldConvertNumberAsIntegerToNumberAsWord() {
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
}
