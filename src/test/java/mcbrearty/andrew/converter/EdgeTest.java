package mcbrearty.andrew.converter;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Test conversion of edge cases.
 * 
 * @author amcbrearty
 */
public class EdgeTest {
	
	private NumberConverter<Integer> integerConverter = new IntegerConverter();

	@Test
	public void convertZero() {
		int numberAsInt = 0;
		String numberAsWord = "zero";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertMax() {
		int numberAsInt = IntegerConverter.MAX_CONVERTIBLE_VALUE;
		String numberAsWord = "nine hundred and ninety nine million nine hundred and ninety nine thousand nine hundred and ninety nine";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertMin() {
		int numberAsInt = IntegerConverter.MIN_CONVERTIBLE_VALUE;
		String numberAsWord = "minus nine hundred and ninety nine million nine hundred and ninety nine thousand nine hundred and ninety nine";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void convertOverMaxLimit() {
		int numberAsInt = IntegerConverter.MAX_CONVERTIBLE_VALUE + 1;
		integerConverter.convert(numberAsInt);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void convertBelowMinLimit() {
		int numberAsInt = IntegerConverter.MIN_CONVERTIBLE_VALUE - 1;
		integerConverter.convert(numberAsInt);
	}
}
