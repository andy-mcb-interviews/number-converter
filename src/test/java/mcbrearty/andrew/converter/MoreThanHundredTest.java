package mcbrearty.andrew.converter;

import static junit.framework.Assert.assertEquals;

import org.junit.Test;

/**
 * Test conversion of positive values over 100.
 * 
 * @author amcbrearty
 */
public class MoreThanHundredTest {

	private NumberConverter<Integer> integerConverter = new IntegerConverter();
	
	@Test
	public void convertOneHundred() {
		int numberAsInt = 100;
		String numberAsWord = "one hundred";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertOneHundredAndOne() {
		int numberAsInt = 101;
		String numberAsWord = "one hundred and one";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertOneThousand() {
		int numberAsInt = 1000;
		String numberAsWord = "one thousand";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertOneThousandAndOne() {
		int numberAsInt = 1001;
		String numberAsWord = "one thousand and one";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertTenThousand() {
		int numberAsInt = 10000;
		String numberAsWord = "ten thousand";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertTenThousandAndOne() {
		int numberAsInt = 10001;
		String numberAsWord = "ten thousand and one";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertOneHundredThousand() {
		int numberAsInt = 100000;
		String numberAsWord = "one hundred thousand";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertOneHundredThousandAndOne() {
		int numberAsInt = 100001;
		String numberAsWord = "one hundred thousand and one";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertOneMillion() {
		int numberAsInt = 1000000;
		String numberAsWord = "one million";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertOneMillionAndOne() {
		int numberAsInt = 1000001;
		String numberAsWord = "one million and one";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertTenMillion() {
		int numberAsInt = 10000000;
		String numberAsWord = "ten million";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertTenMillionAndOne() {
		int numberAsInt = 10000001;
		String numberAsWord = "ten million and one";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertOneHundredMillion() {
		int numberAsInt = 100000000;
		String numberAsWord = "one hundred million";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertOneHundredMillionAndOne() {
		int numberAsInt = 100000001;
		String numberAsWord = "one hundred million and one";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
}
