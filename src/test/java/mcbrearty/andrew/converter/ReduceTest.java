package mcbrearty.andrew.converter;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Test reduce function of the number converter.
 * 
 * @author amcbrearty
 */
public class ReduceTest {

	private IntegerConverter integerConverter = new IntegerConverter();
	 
	@Test
	public void reduceOne() {
		int numberAsInt = 1;
		String numberAsWord = "one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneMillion() {
		int numberAsInt = 1000000;
		String numberAsWord = "one million";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneMillionAndOne() {
		int numberAsInt = 1000001;
		String numberAsWord = "one million and one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneMillionOneHundred() {
		int numberAsInt = 1000100;
		String numberAsWord = "one million one hundred";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneMillionOneHundredAndOne() {
		int numberAsInt = 1000101;
		String numberAsWord = "one million one hundred and one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceTenMillionOneHundredAndOne() {
		int numberAsInt = 10000101;
		String numberAsWord = "ten million one hundred and one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneHundredMillionOneHundredAndOne() {
		int numberAsInt = 100000101;
		String numberAsWord = "one hundred million one hundred and one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceTwentyOne() {
		int numberAsInt = 21;
		String numberAsWord = "twenty one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneHundredAndFive() {
		int numberAsInt = 105;
		String numberAsWord = "one hundred and five";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceFiftySixMillionNineHundredAndFortyFiveThousandSevenHundredAndEightyOne() {
		int numberAsInt = 56945781;
		String numberAsWord = "fifty six million nine hundred and forty five thousand seven hundred and eighty one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceEightyEightThousandNineHundredAndFiftyTwo() {
		int numberAsInt = 88952;
		String numberAsWord = "eighty eight thousand nine hundred and fifty two";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneThousand() {
		int numberAsInt = 1000;
		String numberAsWord = "one thousand";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneThousandAndOne() {
		int numberAsInt = 1001;
		String numberAsWord = "one thousand and one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneThousandAndEleven() {
		int numberAsInt = 1011;
		String numberAsWord = "one thousand and eleven";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneThousandAndTwentyThree() {
		int numberAsInt = 1023;
		String numberAsWord = "one thousand and twenty three";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneThousandSevenHundredAndTwentyThree() {
		int numberAsInt = 1723;
		String numberAsWord = "one thousand seven hundred and twenty three";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceTenThousand() {
		int numberAsInt = 10000;
		String numberAsWord = "ten thousand";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceTenThousandAndOne() {
		int numberAsInt = 10001;
		String numberAsWord = "ten thousand and one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceFiftyThousand() {
		int numberAsInt = 50000;
		String numberAsWord = "fifty thousand";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceFiftyThousandAndOne() {
		int numberAsInt = 50001;
		String numberAsWord = "fifty thousand and one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceFiftyFiveThousand() {
		int numberAsInt = 55000;
		String numberAsWord = "fifty five thousand";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceFiftyFiveThousandAndOne() {
		int numberAsInt = 55001;
		String numberAsWord = "fifty five thousand and one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneHundredThousand() {
		int numberAsInt = 100000;
		String numberAsWord = "one hundred thousand";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneHundredThousandAndOne() {
		int numberAsInt = 100001;
		String numberAsWord = "one hundred thousand and one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceFiveHundredThousand() {
		int numberAsInt = 500000;
		String numberAsWord = "five hundred thousand";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceFiveHundredThousandAndOne() {
		int numberAsInt = 500001;
		String numberAsWord = "five hundred thousand and one";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceTen() {
		int numberAsInt = 10;
		String numberAsWord = "ten";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceSeventy() {
		int numberAsInt = 70;
		String numberAsWord = "seventy";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceSeventySeven() {
		int numberAsInt = 77;
		String numberAsWord = "seventy seven";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneHundredAndSeventy() {
		int numberAsInt = 170;
		String numberAsWord = "one hundred and seventy";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceOneHundredAndSeventyEight() {
		int numberAsInt = 178;
		String numberAsWord = "one hundred and seventy eight";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceNineHundredAndNine() {
		int numberAsInt = 909;
		String numberAsWord = "nine hundred and nine";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceSevenHundredAndFour() {
		int numberAsInt = 704;
		String numberAsWord = "seven hundred and four";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void reduceSevenHundredAndTwentyThree() {
		int numberAsInt = 723;
		String numberAsWord = "seven hundred and twenty three";
		
		StringBuffer buffer = new StringBuffer();
		
		String convertedNumber = integerConverter.reduce(numberAsInt, buffer);
		assertEquals(numberAsWord, convertedNumber);
	}
}
