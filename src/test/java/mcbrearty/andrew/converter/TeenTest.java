package mcbrearty.andrew.converter;

import static junit.framework.Assert.assertEquals;

import org.junit.Test;

/**
 * Test conversion of teens 11,12..17,18 etc.
 * 
 * @author amcbrearty
 */
public class TeenTest {
	
	private NumberConverter<Integer> integerConverter = new IntegerConverter();

	@Test
	public void convertTen() {
		int numberAsInt = 10;
		String numberAsWord = "ten";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertEleven() {
		int numberAsInt = 11;
		String numberAsWord = "eleven";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertTwelve() {
		int numberAsInt = 12;
		String numberAsWord = "twelve";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertThirteen() {
		int numberAsInt = 13;
		String numberAsWord = "thirteen";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertFourteen() {
		int numberAsInt = 14;
		String numberAsWord = "fourteen";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertFifteen() {
		int numberAsInt = 15;
		String numberAsWord = "fifteen";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertSixteen() {
		int numberAsInt = 16;
		String numberAsWord = "sixteen";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertSeventeen() {
		int numberAsInt = 17;
		String numberAsWord = "seventeen";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertEighteen() {
		int numberAsInt = 18;
		String numberAsWord = "eighteen";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	@Test
	public void convertNineteen() {
		int numberAsInt = 19;
		String numberAsWord = "nineteen";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
}
