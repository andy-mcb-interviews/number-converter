package mcbrearty.andrew.converter;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Test conversion of any positive value up to 100.
 * 
 * @author amcbrearty
 */
public class LessThanHundredTest {

	private NumberConverter<Integer> integerConverter = new IntegerConverter();
	
	@Test
	public void convertNinetyNine() {
		int numberAsInt = 99;
		String numberAsWord = "ninety nine";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertFortyNine() {
		int numberAsInt = 49;
		String numberAsWord = "forty nine";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertThirtySeven() {
		int numberAsInt = 37;
		String numberAsWord = "thirty seven";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
	
	@Test
	public void convertEightyTwo() {
		int numberAsInt = 82;
		String numberAsWord = "eighty two";
		String convertedNumber = integerConverter.convert(numberAsInt);
		assertEquals(numberAsWord, convertedNumber);
	}
}
