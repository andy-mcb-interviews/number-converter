# Number Converter
Implementation of a number converter for the conversion of integer numbers into their english equivalent.

## Requirements
* [Aimia Requirements](AimiaRequirements.txt)
* [Ink Requirements](InkRequirements.txt)

## Distribution
* 2015.08.24 - Ink
* 2012.10.18 - Aimia
